Notes:

1) Database Information need to be updated:
--FileName: application.properties

#Provide MYSQL connection URL information for database "springdemo"
spring.datasource.url=jdbc:mysql://localhost:3306/springdemo


#Username 
spring.datasource.username=myspring
#password
spring.datasource.password=myspring

spring.jpa.generate-ddl=true

Created 7 end points:

http://localhost:8080/add
1. /add
(POST)
{
    "name": "Create Project Plan",
    "date": 1592008007000

 }

 You can add any number:
 {
    "name": "Get Approval",
    "date": 1492008007000

 }

 
2) /list
(GET)
Results will be 
[
  {
    "id": 1,
    "name": "Create Project Plan",
    "date": 1592008007000,
    "complete": false,
    "important": false
  },
  {
    "id": 2,
    "name": "Get Approvals",
    "date": 1492008007000,
    "complete": false,
    "important": false
  }
]

You can also verify the results in Database.

3) /update
 (PUT)
Input could be:
{
	"id": 2,
    "name": "Get Approvals",
    "date": 1592008007000

  }

  
 4) /markImportant
 (PUT)
 input:
{
	"id": 2,
    "important": "true"
 }

 5) /markComplete
 (PUT)
 input:
{
	"id": 2,
    "complete": "true"
  }

 6) /delete
 (DEL)
 input:
 2 (just id)
 
 7) /{id}
 (GET)
 ex: 
   /1
   /2 etc
 
 
3) Error Handling:
Due to timeconstraints, error handling is not implemented.